package com.example.resttest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
 
@RestController
public class ListaReversa {
	@GetMapping("/listaReversa")
	public String reverter(@RequestParam(value = "lista") String input) {
		String[] valores = input.replaceAll("\\{", "").replaceAll("\\}", "").split(",");
		String out = "{";
		for (int i = (valores.length)-1; i >= 0; i--) {
	    	out += valores[i];
		    if(i != 0) {
		    	out +=",";
		    }
		}
		out += "}";
		return out;
	}
}
