package com.example.resttest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NomeBibliografico {
	@GetMapping("/nomeBibliografico")
	public String bibliografico (@RequestParam(value = "nome") String input) { // Usar '%20' no request para representar espaços
		String[] nomes = input.split(" ");
		String out = nomes[nomes.length - 1] + ", ";
		for(int i = 0; i < (nomes.length) - 1; i++) {
			out += " " + nomes[i];
		}
		return out;
	}
}
