package com.example.resttest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SistemaMonetario {
	@GetMapping("/sistemaMonetario")
	public String saque (@RequestParam(value = "valor") String input) {
		int valor = Integer.parseInt(input);
		int tmp = valor;
		int notas3 = 0;
		int notas5 = 0;
		while (tmp > 3) {
			if(tmp >= 3 && tmp % 5 != 0) {
				tmp -= 3;
				notas3++;
			} else if(tmp >= 5) {
				tmp -= 5;
				notas5++;
			}
		}
		String out = fazerOutput(valor, notas3, notas5);
		return out;
	}
	private String fazerOutput (int v, int n3, int n5) {
		String str = "Saque R$" + v + ": ";
		
		switch(n3) {
		case 0:
			break;
		case 1:
			str += "1 nota de R$3";
			break;
		default:
			str += n3 + " notas de R$3";
		}
		
		switch(n5) {
		case 0:
			break;
		case 1:
			str += " e 1 nota de R$5";
			break;
		default:
			str += " e " + n5 + " notas de R$5";
		}
		
		return str;
	}
}
