package com.example.resttest;


public class Calculadora {
	public Calculadora () {}
	
	public int mdc(int a, int b){
		while(b != 0) {
			int r = a % b;
			a = b;
			b = r;
		}
		return a;
	}
	
	public int mmc(int a, int b){
		return a * (b / mdc(a, b));
	}

	
	public Fracao soma(Fracao f1, Fracao f2) {
		int novoDenominador = 0;
		if (mdc(f1.getDenominador(), f2.getDenominador()) != 1) {
			novoDenominador = f1.getDenominador() * f2.getDenominador();
		} else {
			novoDenominador = mdc(f1.getDenominador(), f2.getDenominador());
		}
		Fracao resultado = new Fracao();
		resultado.setNumerador( (novoDenominador/f1.getDenominador()) * f1.getNumerador() +
				(novoDenominador/f2.getDenominador()) * f2.getNumerador() );
		resultado.setDenominador(novoDenominador);
		return resultado;
	}
	
	public Fracao subtracao(Fracao f1, Fracao f2) {
		int novoDenominador = 0;
		if (mdc(f1.getDenominador(), f2.getDenominador()) != 1) {
			novoDenominador = f1.getDenominador() * f2.getDenominador();
		} else {
			novoDenominador = mdc(f1.getDenominador(), f2.getDenominador());
		}
		Fracao resultado = new Fracao();
		resultado.setNumerador( (novoDenominador/f1.getDenominador()) * f1.getNumerador() -
				(novoDenominador/f2.getDenominador()) * f2.getNumerador() );
		resultado.setDenominador(novoDenominador);
		return resultado;
	}
	
	public Fracao multiplicacao(Fracao f1, Fracao f2) {
		Fracao resultado = new Fracao();
		resultado.setNumerador(f1.getNumerador() * f2.getNumerador());
		resultado.setDenominador(f1.getDenominador() * f2.getDenominador());
		return resultado;
	}
	
	public Fracao divisao(Fracao f1, Fracao f2) {
		Fracao revert = new Fracao(f2.getDenominador(), f2.getNumerador());
		Fracao resultado = multiplicacao(f1, revert);
		return resultado;
	}
}