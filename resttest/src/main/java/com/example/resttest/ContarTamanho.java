package com.example.resttest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ContarTamanho {
	@GetMapping("/tamanho")
	public String contar(@RequestParam(value = "palavra") String input) {
		int contador = 0;
		for(int i = 0; i < input.length(); i++) {    
            if(input.charAt(i) != ' ')    
                contador++;    
        }
		String out = "tamanho=" + contador;
		return out;
	}
}
