package com.example.resttest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Consoantes {
	@GetMapping("/consoantes")
	public String vogais(@RequestParam(value = "palavra") String input) {
		String out = "";
		char ch;
		for (int i = 0; i < input.length(); i++) {
			  ch  = input.charAt(i);	
			  if(ch!='a' && ch!='e' && ch!='i' && ch!='o' && ch!='u' && ch!='A' && ch!='E' && ch!='I' && ch!='O' && ch!='U') {
				  out += ch;
			  }
		}
		return out;
	}
}
