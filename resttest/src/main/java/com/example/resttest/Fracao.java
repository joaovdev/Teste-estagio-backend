package com.example.resttest;

public class Fracao {
	int numerador;
	int denominador;
	public Fracao() {
	}

	
	public Fracao(int n, int d) {
		numerador = n;
		denominador = d;
	}
	
	public int getNumerador() {
		return numerador;
	}
	
	public void setNumerador(int n) {
		numerador = n;
	}
	
	public int getDenominador() {
		return denominador;
	}
	
	public void setDenominador(int d) {
		denominador = d;
	}
}
