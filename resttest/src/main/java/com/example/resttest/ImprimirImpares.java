package com.example.resttest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ImprimirImpares {
	@GetMapping("/imprimirImpares")
	public String imprImp(@RequestParam(value = "lista") String input) {
		String[] parsed = input.replaceAll("\\{", "").replaceAll("\\}", "").split(",");
		List<String> valores = new ArrayList<>(Arrays.asList(parsed));
		List<String> valoresImpares = new ArrayList<>();
		String out = "{";
		for (int i = 0; i < (valores.size()); i++) {
			if (Integer.parseInt(valores.get(i)) % 2 != 0) {
				valoresImpares.add(valores.get(i));
			}
		}
		out += String.join(",", valoresImpares) + "}";
		return out;
	}
}
