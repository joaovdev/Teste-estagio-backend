package com.example.resttest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Maiusculas {
	@GetMapping("/maiusculas")
	public String contar(@RequestParam(value = "palavra") String input) {
		return input.toUpperCase();
	}
}
